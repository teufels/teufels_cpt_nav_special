##################################
## NAVIGATION SPECIAL CONSTANTS ##
##################################

plugin.tx_teufels_cpt_nav_special {
	settings {
        lib {
            navigation {
                special = 5
                bShowStart = 1
                class {
                    nav = navbar navbar-default navbar-top
                    div = container
                    ul = nav navbar-nav navbar-main
                    li = li
                    a = a
                }
                role {
                    nav = navigation
                }
                optional {
                    active = 1
                }
            }
        }
        production {
            includePath {
                public = EXT:teufels_cpt_nav_special/Resources/Public/
                private = EXT:teufels_cpt_nav_special/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_cpt_nav_special/Resources/Public/
                }

            }
            optional {
                active = 1
            }
        }
    }
}